#!/bin/sh


echo '
## Part Deploy working
##
## @author      Villalba Juan Manuel Pedro <https://github.com/juanma386>
# @Version:     0.0.1
# @Estado:      Alpha
# @Destino:     Deploy Server Php Linux
# @Date:        05:41pm 14-09-2017
# @Advertencia  Usar con cautela y bajo propia responsabilidad
# @Licence:     GPLv2 Free Software Foundation <licensing@fsf.org>
'

_hoy=$(date +"%m_%d_%Y_%r_%S")
_pwd=$(pwd)
_permisos=$(ls -la *)	

PrintLogData(){
cat <<EOF


 ##         ######   ###   ##  ##    ##  ##    ## 
 ##         ######   ###   ##  ##    ##  :##  ##: 
 ##           ##     ###:  ##  ##    ##   ##  ##  
 ##           ##     ####  ##  ##    ##   :####:  
 ##           ##     ##:#: ##  ##    ##    ####   
 ##           ##     ## ## ##  ##    ##    :##:   
 ##           ##     ## ## ##  ##    ##    :##:   
 ##           ##     ## :#:##  ##    ##    ####   
 ##           ##     ##  ####  ##    ##   :####:  
 ##           ##     ##  :###  ##    ##   ##::##  
 ########   ######   ##   ###  :######:  :##  ##: 
 ########   ######   ##   ###   :####:   ##    ## 

EOF
}

bin(){
	if [ -d bin ];
	then
	echo "Sí, sí existe."
	echo "Data is exist in bin folder".	" [ ".$_hoy." ]"
    echo "Data deploy is existed  OK ".	" [ "$_hoy" ]" >> testing.server.log
    echo "Directorio de ./bin existe OK ".	" [ "$_pwd" ]" >> testing.server.log
    echo "Directorio de ./bin OK ".	" [ "$_pwd" ]"
	else
	echo "No, no existe"
	echo "File does not exist"
	echo "Create Directory"
	mkdir bin
        echo "Directorio de bin creado OK ".      " [ "$_pwd" ]" >> testing.server.log
	fi
}
bin
# Creamos el directorio para trabajar donde iran los archivos public_html

public(){
        if [ -d public_html ];
        then
        echo "Sí, sí existe."
        echo "Data is exist in bin folder".     " [ ".$_hoy." ]"
	echo "Data deploy is existed  OK ". " [ "$_hoy" ]" >> testing.server.log
	echo "Directorio de ./publi_html OK ".      " [ "$_pwd" ]" >> testing.server.log
    	echo "Directorio de ./publi_html OK ".     " [ "$_pwd" ]"
        else
        echo "No, no existe"
        echo "File does not exist"
        echo "Create Directory"
	mkdir public_html
	chmod 755 public_html
        echo "Directorio de bin creado OK ".      " [ "$_pwd" ]" >> testing.server.log
        exit
	fi
}
public

createpublic(){
        if [ -d public_html ];
	then
	mkdir public_html
        chmod 775 public_html
	else
	echo "Hay un problema con los permisos, debes resolverlo"
	fi
}
#PrintDataHexome(){ cat >> "EOF" 
#                           EOF}
#PrintDataHexome

# Registro del sistema
registro() {
logger=./testing.server.log
if [ ! -e "$logger" ];
then
echo "Log operative not found init process charging data"
echo '
## Part Deploy working
##
## @author      Villalba Juan Manuel Pedro <https://github.com/juanma386>
# @Version:     0.0.1
# @Estado:      Alpha
# @Destino:     Deploy Server Php Linux
# @Date:        05:41pm 14-09-2017
# @Advertencia  Usar con cautela y bajo propia responsabilidad
# @Licence:     GPLv2 Free Software Foundation <licensing@fsf.org>
' > testing.server.log

    echo "Finish file logger is created and continue process"
else
    echo "Data file log is existed".	" [ ".$_hoy." ]"
    # echo "Los permisos del archivo" $_permisos
    echo "Data file log is existed".    " [ ".$_hoy." ]" >> testing.server.log
    echo "Los permisos del archivo" $_permisos >> testing.server.log
    PrintLogData
fi
}
registro

#Servidor del sistema de despliegue
server() {
servidor=./bin/server.sh
if [ ! -e "$servidor" ];
then
    echo "File does not exist sh"
    echo "Download sh deployer to system_".		"[".$_hoy."]"
    curl -o bin/server.sh https://pastebin.com/raw/RA8w25Vu
    echo 'Downloading finish OK'
    echo 'Verificando Permisos'
    echo "Downloadings data sh to working deploy ".	" [ ".$_hoy." ]" >> testing.server.log 
    dos2unix $_pwd/./*/server.sh
    permisosverificarserver
else
    echo "Data Deploy is existed".	" [ ".$_hoy." ]"
    echo "Data deploy is existed  OK ".	" [ "$_hoy" ]" >> testing.server.log
    echo "Directorio de ejecución OK ".	" [ "$_pwd" ]" >> testing.server.log
    echo "Directorio de ejecución OK ".	" [ "$_pwd" ]"
fi
}
# Permisos del server.sh
permisosverificarserver(){
if [ ! -x "$_pwd/./*/server.sh" ];
then
echo 'Delegando permisos'
echo "Error en permisos del sistema......FAIL".	" [ ".$_hoy." ]" >> testing.server.log 
chmodbin
else
echo 'Permisos de ejecución estan correctos'
echo "Permisos de ejecución................OK".	" [ ".$_hoy." ]" >> testing.server.log 
fi
}
chmodbin(){
chmod +x bin/server.sh
}

server

# Archivo Route del sistema de despliegue

route() {
fileroute=./bin/route.php
if [ ! -e "$fileroute" ]; 
then
    echo "File does not exist"
	echo "Create Directory"
	mkdir bin
    echo "Route data deploy work download".		"[".$_hoy."]"
	echo "Downloading data in progress"
	curl -o bin/route.php https://pastebin.com/raw/dLFnEQvz
	echo "preparing deployer to inicializated ".	" [ ".$_hoy." ]" >> testing.server.log
	permisosverificarroute
else
    echo "Data is exist in bin folder".	" [ ".$_hoy." ]"
    echo "Data deploy is existed  OK ".	" [ "$_hoy" ]" >> testing.server.log
    echo "Directorio de ejecución OK ".	" [ "$_pwd" ]" >> testing.server.log
    echo "Directorio de ejecución OK ".	" [ "$_pwd" ]"

echo 'Listo todo esta instalado y listo para empezar'
exit
fi
}
route
# Ejecutamos la tarea derecho viejo
##./bin/server.sh
unete(){
RNM=$("unete")
reinicios="Despliegue iniciando"
if [ $1 := $RNM ] ; then
echo "El proceso de despliegue ha sido iniciado "$reinicios" es correcto "$1.
./bin/server.sh
else
echo "Para empezar tienes que ejecutar la ./deploy.sh unete"
fi
}
unete

delete(){

RMD=$("borrar")
remover="reinstalado en proceso"
if [ $1 := $RMD ] ; then
echo "El proceso de despliegue ha sido iniciado "$remover" es correcto "$1.
borrado
else
echo "Si quieres actualizar tipea ./deploy.sh borrar"
fi
}
borrardo(){
rm -rfv bin/server.sh
rm -rfv bin/route.php
rm deploy.sh
}
